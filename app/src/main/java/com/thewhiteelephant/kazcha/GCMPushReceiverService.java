package com.thewhiteelephant.kazcha;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;


import com.thewhiteelephant.kazcha.Activities.ArticleDetailActivity;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.Date;

/**
 * Created by hari on 26/6/16.
 */
public class GCMPushReceiverService extends GcmListenerService {

    String id;
    //This method will be called on every new message received
    @Override
    public void onMessageReceived(String from, Bundle data) {

        // Waking up mobile if it is sleeping
        WakeLocker.acquire(getApplicationContext());


        //Getting the message from the bundle
        String message = data.getString("message");
        id = data.getString("id");

        Log.e("ohhh","received");

        Log.e("msg",message);
        Log.e("id",id);
        //Displaying a notiffication with the message

        SharedPreferences sharedPref = getSharedPreferences("preference", Context.MODE_PRIVATE);
        boolean notification = sharedPref.getBoolean("notification",true);
        if (notification) {
            customNotification(message);
//            sendNotification(message);
        }


        // Releasing wake lock
        WakeLocker.release();

    }

    //This method is generating a notification and displaying the notification
    private void sendNotification(String message) {
        Intent intent = new Intent(this, ArticleDetailActivity.class);

        intent.putExtra("from","gcm");
        intent.putExtra("id",id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("കാഴ്ച")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(sound)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_launcher));

//                .setColor(Color.WHITE);;


        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

//        int Unique_Integer_Number=(int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(requestCode, noBuilder.build()); //0 = ID of notification
    }

    public void customNotification(String message) {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.custom_notification);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(this, ArticleDetailActivity.class);
        // Send data to NotificationView Class


        intent.putExtra("from","gcm");
        intent.putExtra("id",id);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setAction(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);


        // Open NotificationView.java Activity
        int requestCode = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        PendingIntent pIntent = PendingIntent.getActivity(this,requestCode, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher)
                // Set Ticker Message
                .setTicker("ticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setSound(sound)
                 .setOngoing(true);;


        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image,R.drawable.ic_launcher);
//        remoteViews.setImageViewResource(R.id.imagenotiright,R.drawable.androidhappy);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title_notify,message);
//        remoteViews.setTextViewText(R.id.text,getString(R.string.customnotificationtext));

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(requestCode, builder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent){

        Log.e("task","removed");
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1,
                restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }


}