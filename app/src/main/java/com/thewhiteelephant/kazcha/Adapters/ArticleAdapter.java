package com.thewhiteelephant.kazcha.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thewhiteelephant.kazcha.Classes.PicassoCallBack;
import com.thewhiteelephant.kazcha.Info.ArticleInfo;


import com.thewhiteelephant.kazcha.R;
import com.thewhiteelephant.kazcha.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 - * Created by hari on 22/6/16.
 - */
public class ArticleAdapter extends ArrayAdapter {
    ArrayList<ArticleInfo> article_list;
//    ArticleInfo item;
    LayoutInflater inflater;
    Context context;
    String font;
    SharedPreferences sharedPref;
    public ArticleAdapter(Context context, ArrayList<ArticleInfo> list) {
        super(context, R.layout.article_item,list);
        article_list=list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;

        sharedPref = context.getSharedPreferences("preference", Context.MODE_PRIVATE);
        font = sharedPref.getString("font","medium");


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ArticleInfo item = article_list.get(position);
        final Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        if (position==0)
        {
            convertView = inflater.inflate(R.layout.header_layout,parent,false);
            TextView title=(TextView)convertView.findViewById(R.id.title);
            ImageView titleImage=(ImageView)convertView.findViewById(R.id.image);
            TextView titleWeb=(TextView)convertView.findViewById(R.id.website);
            TextView titleDate=(TextView)convertView.findViewById(R.id.time);
            ImageView webImage=(ImageView)convertView.findViewById(R.id.web_icon);
            LinearLayout share_layout=(LinearLayout)convertView.findViewById(R.id.share_layout);


            switch (font)
            {
                case "small":  title.setTextSize(16);
                    break;
                case "medium":  title.setTextSize(18);
                    break;
                case "large":  title.setTextSize(20);
                    break;
            }


                        share_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                sharingIntent.setType("text/plain");
                                String shareBody = item.title + "          Read more from "+item.websiteUrl+" shared via Kazcha android app "+context.getResources().getString(R.string.app_url1);
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                            }
                        });

            title.setText(item.title);

            String site;
            switch(item.website)
            {
                case "pathram": site="പത്രം";
                    webImage.setImageResource(R.mipmap.pathram);
                    break;
                case "manorama": site="മനോരമ";
                    webImage.setImageResource(R.mipmap.manorama);
                    break;
                case "eastcoast": site="ഈസ്റ്  കോസ്ററ് ";
                    webImage.setImageResource(R.mipmap.east_coast);
                    break;
                case "mathrubhumi": site="മാതൃഭൂമി";
                    webImage.setImageResource(R.mipmap.mathrubhumi);
                    break;
                case "gizbot": site="ഗിസ്\u200Cബോട്ട്";
                    webImage.setImageResource(R.mipmap.gizbot);
                    break;
                case "filmBeat": site="ഫിലിംബീറ്റ് ";
                    webImage.setImageResource(R.mipmap.film_beat);
                    break;

                case "kazcha": site="കാഴ്ച";
                    webImage.setImageResource(R.mipmap.kazcha);
                    break;
                case "madyamam": site="മാധ്യമം";
                    webImage.setImageResource(R.mipmap.madyamam);
                    break;
                case "mangalam": site="മംഗളം";
                    webImage.setImageResource(R.mipmap.mangalam);
                    break;
                case "marunadan": site="മറുനാടൻ";
                    webImage.setImageResource(R.mipmap.marunadan);
                    break;
                case "oneIndia": site="വൺ ഇന്ത്യ";
                    webImage.setImageResource(R.mipmap.one_india);
                    break;
                default:site=item.website;
            }
            titleWeb.setText(site);


            File file = new File(item.image_path);
            if(file.exists()) {
                Picasso.with(context).load(file).placeholder(null).into(titleImage);
            } else {
                if (!item.imageUrl.equals(""))
                Picasso.with(context).load(item.imageUrl).placeholder(null).into(titleImage, new PicassoCallBack(titleImage,item.imageUrl));

            }
            titleDate.setText(Util.formatDate(item.date));

        }
        else
        {

            convertView = inflater.inflate(R.layout.article_item,parent,false);
            TextView title = (TextView) convertView.findViewById(R.id.articleId);
            TextView website =(TextView) convertView.findViewById(R.id.website);
            ImageView thumbNail=(ImageView) convertView.findViewById(R.id.thumnail);
            TextView dateView=(TextView)convertView.findViewById(R.id.time) ;
            ImageView webImage=(ImageView)convertView.findViewById(R.id.web_icon);


            LinearLayout share_layout=(LinearLayout)convertView.findViewById(R.id.share_layout);

            share_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sharingIntent.setType("text/plain");
                    String shareBody = item.title + "          Read more from "+item.websiteUrl+" shared via Kazcha android app "+context.getResources().getString(R.string.app_url1);
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });



            switch (font)
            {
                case "small":  title.setTextSize(16);
                    break;
                case "medium":  title.setTextSize(18);
                    break;
                case "large":  title.setTextSize(20);
                    break;
            }


            title.setText(item.title);
            String site;
            switch(item.website)
            {
                case "pathram": site="പത്രം";
                    webImage.setImageResource(R.mipmap.pathram);
                    break;
                case "manorama": site="മനോരമ";
                    webImage.setImageResource(R.mipmap.manorama);
                    break;
                case "eastcoast": site="ഈസ്റ്  കോസ്ററ് ";
                    webImage.setImageResource(R.mipmap.east_coast);
                    break;
                case "mathrubhumi": site="മാതൃഭൂമി";
                    webImage.setImageResource(R.mipmap.mathrubhumi);
                    break;
                case "gizbot": site="ഗിസ്\u200Cബോട്ട്";
                    webImage.setImageResource(R.mipmap.gizbot);
                    break;
                case "filmBeat": site="ഫിലിംബീറ്റ് ";
                    webImage.setImageResource(R.mipmap.film_beat);
                    break;

                case "kazcha": site="കാഴ്ച";
                    webImage.setImageResource(R.mipmap.kazcha);
                    break;
                case "madyamam": site="മാധ്യമം";
                    webImage.setImageResource(R.mipmap.madyamam);
                    break;
                case "mangalam": site="മംഗളം";
                    webImage.setImageResource(R.mipmap.mangalam);
                    break;
                case "marunadan": site="മറുനാടൻ";
                    webImage.setImageResource(R.mipmap.marunadan);
                    break;
                case "oneIndia": site="വൺ ഇന്ത്യ";
                    webImage.setImageResource(R.mipmap.one_india);
                    break;
                default:site=item.website;
            }
            website.setText(site);
            dateView.setText(Util.formatDate(item.date));
            File file = new File(item.thubnail_path);
            if(file.exists()) {
                Picasso.with(context).load(file).into(thumbNail);
            } else {
                if (!item.thumbnailUrl.equals(""))
                Picasso.with(context).load(item.thumbnailUrl).placeholder(null).into(thumbNail, new PicassoCallBack(thumbNail,item.thubnail_path));

            }
        }
        return convertView;
    }

}
