package com.thewhiteelephant.kazcha.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thewhiteelephant.kazcha.Activities.MainActivity;
import com.thewhiteelephant.kazcha.Activities.SettingsActivity;
import com.thewhiteelephant.kazcha.Classes.PicassoCallBack;
import com.thewhiteelephant.kazcha.Info.ArticleInfo;

import com.thewhiteelephant.kazcha.R;
import com.thewhiteelephant.kazcha.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hari on 7/7/16.
 */
public class DetailPageAdapter extends PagerAdapter {

    private Activity context;
    ArrayList<ArticleInfo> articleList;
    String font;
    SharedPreferences sharedPref;
    int count;
    String gcm;


    public DetailPageAdapter(Activity activity, ArrayList<ArticleInfo> arrayList,String gcm)
    {
        this.context=activity;
        this.articleList=arrayList;
        sharedPref = context.getSharedPreferences("preference", Context.MODE_PRIVATE);
        font = sharedPref.getString("font","medium");
        count=arrayList.size();
        this.gcm=gcm;

    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view==((CoordinatorLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){

        final ArticleInfo articleInfo=articleList.get(position);
        View view;
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(R.layout.activity_article_detail,container,false);
        ImageView imageView=(ImageView)view.findViewById(R.id.imageView);
        TextView titleView=(TextView)view.findViewById(R.id.title);
        TextView contentView=(TextView)view.findViewById(R.id.content);
        TextView websitetView=(TextView)view.findViewById(R.id.website);
        TextView websiteUrltView=(TextView)view.findViewById(R.id.url);
        ImageView webIcon=(ImageView)view.findViewById(R.id.web_icon);
        TextView titleDate=(TextView)view.findViewById(R.id.time);
        titleView.setText(articleInfo.title);
        contentView.setText(articleInfo.content);
        LinearLayout share=(LinearLayout)view.findViewById(R.id.share_layout);
        ImageView back=(ImageView)view.findViewById(R.id.back);
        ImageView settings=(ImageView)view.findViewById(R.id.settings);
        LinearLayout stripLayout=(LinearLayout)view.findViewById(R.id.strip);

        switch (font)
        {
            case "small":  titleView.setTextSize(17);
                contentView.setTextSize(16);
                break;
            case "medium":  titleView.setTextSize(19);
                contentView.setTextSize(18);
                break;
            case "large":  titleView.setTextSize(21);
                contentView.setTextSize(20);
                break;
        }
        boolean night = sharedPref.getBoolean("switch_night", false);

        if (night)
        {
            Log.e("night","true");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//            ((GradientDrawable)stripLayout.getBackground()).setColor(0xFF333238);


        }
        else
        {
            Log.e("night","false");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//            ((GradientDrawable)stripLayout.getBackground()).setColor(0xFFFFFFFF);
        }

        String site;
        switch(articleInfo.website)
        {
            case "pathram": site="പത്രം";
                webIcon.setImageResource(R.mipmap.pathram);
                break;
            case "manorama": site="മനോരമ";
                webIcon.setImageResource(R.mipmap.manorama);
                break;
            case "eastcoast": site="ഈസ്റ്  കോസ്ററ് ";
                webIcon.setImageResource(R.mipmap.east_coast);
                break;
            case "mathrubhumi": site="മാതൃഭൂമി ";
                webIcon.setImageResource(R.mipmap.mathrubhumi);
                break;
            case "gizbot": site="ഗിസ്\u200Cബോട്ട്";
                webIcon.setImageResource(R.mipmap.gizbot);
                break;
            case "filmBeat": site="ഫിലിംബീറ്റ് ";
                webIcon.setImageResource(R.mipmap.film_beat);
                break;

            case "kazcha": site="കാഴ്ച";
                webIcon.setImageResource(R.mipmap.kazcha);
                break;
            case "madyamam": site="മാധ്യമം";
                webIcon.setImageResource(R.mipmap.madyamam);
                break;
            case "mangalam": site="മംഗളം";
                webIcon.setImageResource(R.mipmap.mangalam);
                break;
            case "marunadan": site="മറുനാടൻ";
                webIcon.setImageResource(R.mipmap.marunadan);
                break;
            case "oneIndia": site="വൺ ഇന്ത്യ";
                webIcon.setImageResource(R.mipmap.one_india);
                break;
            default:site=articleInfo.website;
        }

        websitetView.setText(site);
        titleDate.setText(Util.dateInMont(articleInfo.date));

        File file = new File(articleInfo.image_path);
        if(file.exists()) {
            Picasso.with(context).load(file).placeholder(null).into(imageView);
        } else {
            if (!articleInfo.imageUrl.equals(""))
            Picasso.with(context).load(articleInfo.imageUrl).placeholder(null).into(imageView, new PicassoCallBack(imageView,articleInfo.image_path));
        }

        websiteUrltView.setText("തുടർന്നു വായിക്കുക ");

        websiteUrltView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(articleInfo.websiteUrl));
                context.startActivity(browserIntent);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("share","share");
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");


                String shareBody = articleInfo.title + "          Read more from "+articleInfo.websiteUrl+" shared via Kazcha android app "+context.getResources().getString(R.string.app_url1);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (gcm.equals("gcm"))
                {
                    Intent intent=new Intent(context,MainActivity.class);
                    context.startActivity(intent);
                    context.finish();

                }
                else
                {
                    context.finish();
                }

            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(context,SettingsActivity.class);
                context.startActivity(intent);


            }
        });

        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container,int position,Object obj)
    {
        container.removeView((CoordinatorLayout) obj);
    }

    // Method to share either text or URL.
    private void shareTextUrl() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "http://www.google.com");

        context.startActivity(Intent.createChooser(share, "Share link!"));
    }
}
