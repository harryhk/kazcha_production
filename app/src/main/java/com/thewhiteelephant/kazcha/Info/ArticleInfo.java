package com.thewhiteelephant.kazcha.Info;

import com.thewhiteelephant.kazcha.Classes.DBHelper;
import com.thewhiteelephant.kazcha.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by hari on 21/6/16.
 */
public class ArticleInfo implements Comparable<ArticleInfo> {

    public int articleId,category;
    public String title,content,imageUrl,thumbnailUrl,website, websiteUrl,date, thubnail_path,image_path;
    public Date dateTime;

    public static ArticleInfo parseArticle(JSONObject jsonObject,DBHelper dbHelper)
    {
        ArticleInfo articleInfo=new ArticleInfo();
        try {
            articleInfo.articleId=jsonObject.getInt("articleId");
            articleInfo.title=jsonObject.getString("title");
            articleInfo.content=jsonObject.getString("content");
            articleInfo.imageUrl=jsonObject.getString("imageUrl");
            articleInfo.thumbnailUrl=jsonObject.getString("thumbnailUrl");
            articleInfo.website=jsonObject.getString("website");
            articleInfo.websiteUrl =jsonObject.getString("websiteurl");
            articleInfo.category=jsonObject.getInt("category");
            articleInfo.date=jsonObject.getString("date");
            articleInfo.dateTime=Util.stringToDate(articleInfo.date);
            articleInfo.thubnail_path = AppInfo.appPath+"articles/thumbnai"+articleInfo.articleId+".jpg";
            articleInfo.image_path = AppInfo.appPath+"articles/image"+articleInfo.articleId+".jpg";

            if (!dbHelper.articleExist(articleInfo.articleId))
            dbHelper.insertArticle(articleInfo);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return articleInfo;
    }

    public Date getDateTime() {
        return dateTime;
    }



    @Override
    public int compareTo(ArticleInfo another) {
        if (getDateTime() == null || another.getDateTime() == null)
            return 0;
        return getDateTime().compareTo(another.getDateTime());
    }
}
