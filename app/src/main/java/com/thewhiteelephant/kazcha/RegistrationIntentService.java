package com.thewhiteelephant.kazcha;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by hari on 26/6/16.
 */
public class RegistrationIntentService extends IntentService {
    public RegistrationIntentService(String name) {
        super(name);

        Log.e("intent","service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        Log.e("handle","intent");
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
