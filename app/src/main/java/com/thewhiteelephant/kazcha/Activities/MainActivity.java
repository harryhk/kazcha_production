package com.thewhiteelephant.kazcha.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.thewhiteelephant.kazcha.Adapters.ArticleAdapter;
import com.thewhiteelephant.kazcha.Fragment.ContentFragment;
import com.thewhiteelephant.kazcha.Info.AppInfo;
import com.thewhiteelephant.kazcha.Classes.DBHelper;
import com.thewhiteelephant.kazcha.Info.ArticleInfo;
import com.thewhiteelephant.kazcha.R;
import com.thewhiteelephant.kazcha.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,ContentFragment.OnHeadlineSelectedListener {
    Activity activity;
    ArticleInfo articleInfo;
    DBHelper dbHelper;
//    ArrayList<ArticleInfo> article_list;
    public static ArrayList<ArticleInfo> article_list_db=new ArrayList<>();
    ArticleAdapter articleAdapter;
    ListView list_article;
    private SwipeRefreshLayout swipeRefreshLayout;
    static int INITIAL=0,RELOAD=1;
    private int preLast;
    JSONObject object;
    public static int NEW=0,INDIA=1,TECHNOLOGY=2,CINEMA=3,SPORTS=4,BUSINESS=5,PRAVASI=6;
    private static int catagory=NEW;
     ProgressBar progressBar;
    View footerView,headerView;
    TextView footer,title,titleDate,titleWeb;
    ImageView titleImage;
    ProgressBar footer_progress;
    ImageView reloadButton;
    RelativeLayout noContentLayout;
    AdView mAdView;
//    LinearLayout share_layout;
    public static boolean modeChange=false;
    public static boolean resume;
    String latest;
    long rate_gap;
    boolean rated;
    SharedPreferences sharedpreferences;
    long prev_launch,current_launch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-8564832003866759~6533075428");
        activity=this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mAdView = (AdView) findViewById(R.id.adView);
        sharedpreferences = getSharedPreferences("preference", Context.MODE_PRIVATE);
        boolean night = sharedpreferences.getBoolean("switch_night", false);
        latest= sharedpreferences.getString("latest_version","1.0");
        rate_gap= sharedpreferences.getLong("rate_gap",0);
        rated= sharedpreferences.getBoolean("rated", false);
        resume=false;


        Calendar now = Calendar.getInstance();
        prev_launch=now.getTimeInMillis();



        if (night)
        {
            Log.e("night","true");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else
        {
            Log.e("night","false");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        setSupportActionBar(toolbar);
        dbHelper=new DBHelper(activity);
        articleInfo=new ArticleInfo();
        article_list_db=new ArrayList<>();
        object=new JSONObject();
        switch (catagory)
        {
            case 0:getSupportActionBar().setTitle("ഏറ്റവും പുതിയത് ");
                break;
            case 1:getSupportActionBar().setTitle("ഇന്ത്യ");
                break;
            case 2:getSupportActionBar().setTitle("ടെക്നോളജി");
                break;
            case 3:getSupportActionBar().setTitle("സിനിമ");
                break;
            case 4:getSupportActionBar().setTitle("സ്പോർട്സ്");
                break;
            case 5:getSupportActionBar().setTitle("ബിസിനെസ്സ് " );
                break;
            case 6:getSupportActionBar().setTitle("പ്രവാസി");
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onRestart(){
        super.onRestart();
        Log.e("on","restart");
        if (modeChange)
        recreate();

        if(!resume){
            Calendar now = Calendar.getInstance();
            current_launch=now.getTimeInMillis();

            long difference=current_launch-prev_launch;
            long sec=difference/1000;
            long min=sec/60;

            Log.e("diffResume",String.valueOf(min));
            if (min>=1)
            {
                prev_launch=current_launch;
                progressBar.setVisibility(View.VISIBLE);
                onRefresh();
            }


        }


        resume=false;
    }


    @Override
    public void getSwipeRefreshLayout(SwipeRefreshLayout swipehLayout) {
        swipeRefreshLayout=swipehLayout;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.splash, menu);
//        MenuItem item = menu.findItem(R.id.action_settings);
//        item.setVisible(false);
//        invalidateOptionsMenu();
        Log.e("menuu","ccclick");
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("click","option");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent=new Intent(activity,SettingsActivity.class);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_new) {
            catagory=NEW;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("ഏറ്റവും പുതിയത് ");
                object =new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","0");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL, AppInfo.url+"Kazcha/fetchArticles.php",object);

        } else if (id == R.id.nav_india) {

            catagory=INDIA;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("ഇന്ത്യ");
                object =new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);


        } else if (id == R.id.nav_technology) {

            catagory=TECHNOLOGY;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("ടെക്നോളജി");
                object=new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","2");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);

        } else if (id == R.id.nav_cinema) {

            catagory=CINEMA;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("സിനിമ");
                object =new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","3");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);

        } else if (id == R.id.nav_sports) {

            catagory=SPORTS;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("സ്പോർട്സ്");
                object =new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","4");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);

        } else if (id == R.id.nav_business) {

            catagory=BUSINESS;
            try {
                hideFooter();
                article_list_db.clear();
                article_list_db=dbHelper.getAllArticles(catagory);
                articleAdapter.notifyDataSetChanged();
                getSupportActionBar().setTitle("ബിസിനസ്സ്" );
                object =new JSONObject();
                object.put("articleId","0");
                object.put("categoryId","5");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.VISIBLE);
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);

        }
//        else if (id == R.id.nav_pravasi) {
//
//            catagory=PRAVASI;
//            try {
//                article_list_db.clear();
//                article_list_db=dbHelper.getAllArticles(catagory);
//                articleAdapter.notifyDataSetChanged();
//                getSupportActionBar().setTitle("പ്രവാസി");
//                object = new JSONObject();
//                object.put("articleId","0");
//                object.put("categoryId","6");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            progressBar.setVisibility(View.VISIBLE);
//            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void hideFooter()
    {
        footer.setText("");
        footer_progress.setVisibility(View.GONE);
    }

    public  void postString(final Activity activity, final int attempt, String url, final JSONObject jsonObject)
    {
        Log.e("url",url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        if (!rated)
                        {
                            rate();
                        }
                            getArticleResult(response,attempt);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("result",error.toString());
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(activity,"Network Not Available!",Toast.LENGTH_SHORT).show();
                      getOfflineData();

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext())
                {
                    try {
                        Object element = keys.next();
                        String key=element.toString();
                        String value=jsonObject.getString(element.toString());
                        Log.e("key",key);
                        Log.e("value",value);
                        params.put(key,value);
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.e("exception","json");
                    }
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    public void checkContentAvailable()
    {
        if (dbHelper.getArticleCount1(catagory)==0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        {
            Log.e("db","novalue");
            footerView.setVisibility(View.GONE);
            footer.setText("");
            footer_progress.setVisibility(View.GONE);
            noContentLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            noContentLayout.setVisibility(View.GONE);
        }
    }

    public void getOfflineData()
    {
        Log.e("get","offline");
        progressBar.setVisibility(View.GONE);
        checkContentAvailable();
        article_list_db=dbHelper.getAllArticles(catagory);
        sortArticle();
        articleAdapter=new ArticleAdapter(activity,article_list_db);
        list_article.setAdapter(articleAdapter);
        swipeRefreshLayout.setRefreshing(false);
        footer.setText("Network not available !");
        footer_progress.setVisibility(View.GONE);
    }

    public int getOldestId()
    {
        int i,id=0;
        for (i=0;i<article_list_db.size();i++)
        {
         id=article_list_db.get(i).articleId;
        }
        return id;
    }


    public void update(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("A new version of kazcha is available.Would you like to update the app?");

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.app_url)));
               startActivity(browserIntent);
            }
        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });



        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



    void rateAlert()
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("കാഴ്ച റേറ്റ് ചെയ്യൂ ");

        alertDialogBuilder.setPositiveButton("          ശരി ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("rated",true);
                editor.commit();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.app_url)));
                startActivity(browserIntent);
            }
        });

        alertDialogBuilder.setNeutralButton("പിന്നീട് ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogBuilder.setNegativeButton("ഒരിക്കലുമില്ല",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("rated",true);
                editor.commit();
//                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void rate()
    {
        Calendar now = Calendar.getInstance();

        rate_gap= sharedpreferences.getLong("rate_gap",now.getTimeInMillis());
        long difference=now.getTimeInMillis()-rate_gap;
        long sec=difference/1000;
        long min=sec/60;
        long hour=min/60;
        long day=hour/24;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong("rate_gap",rate_gap);
        editor.commit();

        Log.e("rated_perios",String.valueOf(day));
        if ((day>6) && (day<30))
        {

            SharedPreferences.Editor editor1 = sharedpreferences.edit();
            editor1.putLong("rate_gap", now.getTimeInMillis());
            editor1.commit();
            rateAlert();
        }

    }


    public  void  getArticleResult(String response,int attempt)
    {
        Log.e("jsonResult",response);
        try {
            JSONObject jsonObject=new JSONObject(response);
            String status=jsonObject.getString("status");
            String version=jsonObject.getString("version");
            String statusMessage=jsonObject.getString("statusMessage");
            Log.e("status",status);

            if (!version.equals(latest))
            {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("latest_version",version);
                editor.commit();
                latest= sharedpreferences.getString("latest_version","1.0");
                update();
            }
            if (statusMessage.equals("No more feeds available")){
                footer.setText("No more contents to load !");
                footer_progress.setVisibility(View.GONE);
            }
            else
            if (status.equals("success")&& attempt==INITIAL){
                dbHelper.deleteAllArticles();
                article_list_db.clear();
                preLast=0;
                Log.e("db","clrd");

            }
            JSONArray jsonArray=jsonObject.getJSONArray("articles");
            for (int i=0;i<jsonArray.length();i++)
            {

                JSONObject object=jsonArray.getJSONObject(i);
                ArticleInfo articleInfo=ArticleInfo.parseArticle(object,dbHelper);
                article_list_db.add(articleInfo);
            }

            Log.e("dbCount",String.valueOf(dbHelper.getArticleCount()));
            checkContentAvailable();

        } catch (JSONException e) {
            e.printStackTrace();

            Log.e("jsonExcep","getArtCount");
        }

        sortArticle();

        if (attempt==INITIAL)
        {
            articleAdapter=new ArticleAdapter(activity,article_list_db);
            list_article.setAdapter(articleAdapter);
        }
        else
        {
                articleAdapter.notifyDataSetChanged();
                list_article.invalidate();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        Log.e("on","refresh");

        rated= sharedpreferences.getBoolean("rated", false);
//        latest= sharedpreferences.getString("latest_version","1.0");
//        rate_gap= sharedpreferences.getLong("rate_gap",0);
        object=new JSONObject();
        try {
            object.put("articleId","0");

                object.put("categoryId",String.valueOf(catagory));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (Util.isDeviceOnline(activity))
        {
             final int lastItem = firstVisibleItem + visibleItemCount;

            Log.e("totCount",String.valueOf(totalItemCount));
            Log.e("lasttitem",String.valueOf(lastItem));
            Log.e("prelast",String.valueOf(preLast));
                if(lastItem == totalItemCount) {
                    if(preLast!=lastItem){ //to avoid multiple calls for last item
                        Log.d("Last", "Last");
                        Log.e("total",String.valueOf(String.valueOf(dbHelper.getArticleCount())));
                        object=new JSONObject();
                        try {
                            object.put("categoryId",catagory);
                            object.put("articleId",String.valueOf(getOldestId()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        footer.setText("No more contents to load !");
//                        footer_progress.setVisibility(View.GONE);
                        if (totalItemCount>4)
                        {
                            Log.e("tot20","test");
                            footerView.setVisibility(View.VISIBLE);
                            footer.setText("Loading Contents");
                            footer_progress.setVisibility(View.VISIBLE);
                            postString(activity,RELOAD,AppInfo.url+"Kazcha/fetchOldArticles.php",object);

                        }
                        preLast = lastItem;

                    }
                }
        }
        else
        {
            preLast=0;
        }
    }

    @Override
    public void getProgressbar(ProgressBar progres) {
        progressBar=progres;
    }

    @Override
    public void getReloadLayout(ImageView button,RelativeLayout linearLayout) {
        reloadButton=button;
        noContentLayout=linearLayout;
    }

    @Override
    public void mreCreate() {
        try {
            object = new JSONObject();
            object.put("articleId","0");
            object.put("categoryId",String.valueOf(catagory));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressBar.setVisibility(View.VISIBLE);
        postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);
    }

    private void sortArticle()
    {
        Collections.sort(article_list_db, new Comparator<ArticleInfo>() {
            public int compare(ArticleInfo o2, ArticleInfo o1) {
                if (o1.getDateTime() == null || o2.getDateTime() == null)
                    return 0;
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });
    }

    @Override
    public void onArticleSelected(final int position, View view) {
       modeChange=false;
//        share_layout=(LinearLayout) view.findViewById(R.id.share_layout);
        Intent intent=new Intent(activity,ArticleDetailActivity.class);
        intent.putExtra("position",position);
//        intent.putExtra("mylist", article_list_db);
        startActivity(intent);
    }

    @Override
    public void getListView(ListView listView) {
        list_article=listView;
        footerView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
        headerView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header_layout, null, false);
        list_article.addFooterView(footerView,null,false);
        footer=(TextView)footerView.findViewById(R.id.footer_1);
        footer_progress=(ProgressBar)footerView.findViewById(R.id.footer_progress);

        if (Util.isDeviceOnline(activity))
        {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
            article_list_db=dbHelper.getAllArticles(catagory);
            sortArticle();
            articleAdapter=new ArticleAdapter(activity,article_list_db);
            list_article.setAdapter(articleAdapter);
            try {
                object.put("articleId","0");
                object.put("categoryId",catagory);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postString(activity,INITIAL,AppInfo.url+"Kazcha/fetchArticles.php",object);
        }
        else
        {
            getOfflineData();
        }
    }
}
