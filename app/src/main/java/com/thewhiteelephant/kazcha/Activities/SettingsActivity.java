package com.thewhiteelephant.kazcha.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.thewhiteelephant.kazcha.AppController;
import com.thewhiteelephant.kazcha.R;

public class SettingsActivity extends AppCompatActivity {

    TextView small,medium,large;
    public static final int ON       = 0xFFF50057;
    public static final int NIGHT      = 0xFFFFFFFF;
    public static final int DAY        = 0xFF000000;
    SharedPreferences sharedPref;
    Context context;
    boolean night,notification;
    String font;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_settings);
        small=(TextView)findViewById(R.id.small);
        medium=(TextView)findViewById(R.id.medium);
        large=(TextView)findViewById(R.id.large);
        context=this;
        sharedPref = context.getSharedPreferences("preference", Context.MODE_PRIVATE);

        night = sharedPref.getBoolean("switch_night", false);


        font = sharedPref.getString("font","medium");
        MainActivity.resume=true;
        MainActivity.modeChange=false;


        if (night)
        {
            Log.e("night","true");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else
        {
            Log.e("night","false");
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }


        Switch switch_night,switch_notification;

        switch_night=(Switch)findViewById(R.id.switch_night);
        switch_notification=(Switch)findViewById(R.id.switch_notification);

         night = sharedPref.getBoolean("switch_night", false);
         notification = sharedPref.getBoolean("notification",true);
        if (night)
        {
            switch_night.setChecked(true);

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else
        {
            switch_night.setChecked(false);

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        switch (font)
        {
            case "small": ((GradientDrawable)small.getBackground()).setColor(ON);
                break;
            case "medium": ((GradientDrawable)medium.getBackground()).setColor(ON);
                break;
            case "large": ((GradientDrawable)large.getBackground()).setColor(ON);
                break;
        }

        if (notification)
        {
            switch_notification.setChecked(true);

        }
        else
        {
            switch_notification.setChecked(false);

        }
        switch_night.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                MainActivity.modeChange=true;

                Log.e("s_night","test");

                if (isChecked)
                {
                    Log.e("s_night","true");
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("switch_night",true);
                    editor.commit();
                    AppController.nightMode();
//                    night=true;

//                    recreate();
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                else
                {
                    Log.e("s_night","false");
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("switch_night", false);
                    editor.commit();
                    AppController.daytMode();
//                    night=false;
//                    recreate();
//                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });

        switch_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("notification",true);
                    editor.commit();
                }
                else
                {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("notification",false);
                    editor.commit();

                }
            }
        });
    }


    public void end(View v)
    {
        finish();
    }
    public void small(View v)
    {
        MainActivity.modeChange=true;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("font","small");
        editor.commit();

        ((GradientDrawable)v.getBackground()).setColor(ON);


            Log.e("switch","day");
            ((GradientDrawable)medium.getBackground()).setColor(DAY);
            ((GradientDrawable)large.getBackground()).setColor(DAY);

    }
    public void medium(View v)
    {

        MainActivity.modeChange=true;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("font","medium");
        editor.commit();
        ((GradientDrawable)v.getBackground()).setColor(ON);


            Log.e("switch","day");
            ((GradientDrawable)small.getBackground()).setColor(DAY);
            ((GradientDrawable)large.getBackground()).setColor(DAY);

    }
    public void large(View v)
    {

        MainActivity.modeChange=true;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("font","large");
        editor.commit();

        ((GradientDrawable)v.getBackground()).setColor(ON);

            Log.e("switch","day");
            ((GradientDrawable)medium.getBackground()).setColor(DAY);
            ((GradientDrawable)small.getBackground()).setColor(DAY);

    }


    public void feedback(View view)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","labsazync@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Kazcha App feedback");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Type Your message here :)");
        startActivity(Intent.createChooser(emailIntent, "Send email.."));
    }

    public void share(View v)
    {
        Log.e("share","share");
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "കണ്ടു തീരാത്ത കാഴ്ചകൾ, ഡൗൺലോഡ് ചെയ്യൂ കാഴ്ച മലയാളം ന്യൂസ് ആപ്പ്  "+getResources().getString(R.string.app_url1);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void rate(View v)
    {

        Uri uri = Uri.parse(getResources().getString(R.string.app_url));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        try {
            this.startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            // Hmm, market is not installed
            Log.w("TAG", "Android Market is not installed");
        }
    }
}
