package com.thewhiteelephant.kazcha.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.thewhiteelephant.kazcha.Classes.DBHelper;
import com.thewhiteelephant.kazcha.GCMRegistrationIntentService;
import com.thewhiteelephant.kazcha.Info.AppInfo;
import com.thewhiteelephant.kazcha.R;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
            sharedpreferences = getSharedPreferences("preference", Context.MODE_PRIVATE);
        activity=this;



            boolean initial = sharedpreferences.getBoolean("initial", true);

            boolean night = sharedpreferences.getBoolean("switch_night", false);
        if (night)
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }



        String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File dfile = new File(sdPath + "/Kazcha/");
        dfile.mkdir();

        if (initial)
        {
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }


        (new File(sdPath + "/Kazcha/articles/")).mkdirs();

        AppInfo.appPath = sdPath + "/Kazcha/";
        File file = new File(AppInfo.appPath + ".nomedia");

        DBHelper dbHelper=new DBHelper(this);

        if (dbHelper.getArticleCount()>40)
        {
            if (file.exists())
                file.delete();
        }

            try {
                file.createNewFile();

            } catch (IOException e) {
                e.printStackTrace();

                Log.e("endat","splash");
            }


        Bundle extras = getIntent().getExtras();
        if(extras!=null && extras.containsKey("crash")) {
            String crashReport = extras.getString("crash");
            String model=Build.MODEL;
            String version=Build.VERSION.RELEASE;
          sendCrashReport(activity,"http://newsatglance.com/Kazcha/CrashLogs/registerCrashLog.php",crashReport,model,version);

        }


        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                Intent intent=new Intent(activity,MainActivity.class);
                startActivity(intent);

                finish();

            }

        }.start();

    }

    public  void sendCrashReport(final Activity activity, String url, final String value, final String model, final String version)
    {
        Log.e("url",url);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       Log.e("crashResponse",response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorCrashresult",error.toString());

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();


                        params.put("deviceModel",model);
                        params.put("osVersion",version);
                        params.put("log",value);


                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }
}
