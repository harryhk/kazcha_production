package com.thewhiteelephant.kazcha.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.thewhiteelephant.kazcha.Adapters.DetailPageAdapter;
import com.thewhiteelephant.kazcha.Classes.DBHelper;
import com.thewhiteelephant.kazcha.Info.ArticleInfo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.thewhiteelephant.kazcha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ArticleDetailActivity extends AppCompatActivity {


    Activity context;
    int position;
    DetailPageAdapter detailPageAdapter;
    ViewPager viewPager;
    DBHelper dbHelper;
    ProgressBar progressBar;
    AdView mAdView;
    ImageView swipe_alert;
    SharedPreferences sharedPreferences;
    boolean alerted;
    ArrayList<ArticleInfo> articleInfos;
    String from;
    ImageView reload_page;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);



        setContentView(R.layout.activity_screen_slide);
        viewPager=(ViewPager)findViewById(R.id.pager);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        swipe_alert=(ImageView)findViewById(R.id.swipe);
        reload_page=(ImageView)findViewById(R.id.reload);
        context=this;
        dbHelper=new DBHelper(context);
        mAdView = (AdView) findViewById(R.id.adView);
        reload_page.setVisibility(View.GONE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        MainActivity.resume=true;

        sharedPreferences = getSharedPreferences("preference", Context.MODE_PRIVATE);
        alerted = sharedPreferences.getBoolean("alerted", false);
        articleInfos=new ArrayList<>();
        if (!alerted)
            swipe_alert.setVisibility(View.VISIBLE);



        Bundle extras = getIntent().getExtras();
        from="";
        Intent intent=this.getIntent();
        if(intent !=null){
            from  = intent.getStringExtra("from");
        }
        if ((from!=null)&&from.equals("gcm"))
        {
            progressBar.setVisibility(View.VISIBLE);
            Integer id = Integer.parseInt(extras.getString("id"));
            JSONObject object=new JSONObject();
            try {

                object =new JSONObject();
                object.put("articleId",id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postString(context,"http://www.newsatglance.com/Kazcha/fetchArticleDetails.php",object);
        }
        else
        {
            detailPageAdapter=new DetailPageAdapter(context,MainActivity.article_list_db,"not_gcm");
            viewPager.setAdapter(detailPageAdapter);
            position  = extras.getInt("position",0);
            viewPager.setCurrentItem(position);

        }
    }


    @Override
    public void onRestart(){
        super.onRestart();
        Log.e("on","restart");
        recreate();
    }


//    @Override
//    public void onResume(){
//        super.onResume();
//        if (from.equals("gcm"))
//        {
//            finish();
//        }
//    }



    public  void postString(final Activity activity, String url, final JSONObject jsonObject)
    {

        Log.e("url",url);


        StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String status=jsonObject.getString("status");
                            if (status.equals("success"))
                            {
                                reload_page.setVisibility(View.GONE);

                                JSONObject object=jsonObject.getJSONObject("articles");
                                ArticleInfo articleInfo=ArticleInfo.parseArticle(object,dbHelper);
                                articleInfos.add(articleInfo);

                                detailPageAdapter=new DetailPageAdapter(context,articleInfos,"gcm");

                                viewPager.setAdapter(detailPageAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("result",error.toString());
                        progressBar.setVisibility(View.GONE);
                        reload_page.setVisibility(View.VISIBLE);
                        Toast.makeText(activity,"Network Not Available!",Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();


                Iterator<String> keys = jsonObject.keys();

                while (keys.hasNext())
                {
                    try {
                        Object element = keys.next();
                        String key=element.toString();
                        String value=jsonObject.getString(element.toString());
                        Log.e("key",key);
                        Log.e("value",value);
                        params.put(key,value);
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.e("exception","json");
                    }
                }
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);


    }


    public void swipeAlert(View view)
    {
        Log.e("help","err");
       view.setVisibility(View.GONE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("alerted",true);
        editor.commit();
    }



    public void reload(View view)
    {
       recreate();
    }
}
