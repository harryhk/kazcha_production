package com.thewhiteelephant.kazcha.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.thewhiteelephant.kazcha.R;


/**
 * Created by hari on 27/6/16.
 */
public class ContentFragment  extends ListFragment  implements AdapterView.OnItemClickListener,SwipeRefreshLayout.OnRefreshListener {


    OnHeadlineSelectedListener mCallback;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;
    ImageView reload;
    RelativeLayout noContentLayout;
    LinearLayout share_layout;



    @Override
    public void onRefresh() {

        mCallback.onRefresh();
    }



    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position,View view);

        public void getListView(ListView listView );
        public void onRefresh();
        public void getSwipeRefreshLayout(SwipeRefreshLayout swipeLayout);
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount);
        public void getProgressbar(ProgressBar progressBar);
        public void getReloadLayout(ImageView button,RelativeLayout linearLayout);
        public void mreCreate();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
       swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        progressBar=(ProgressBar)view.findViewById(R.id.progressbar);
        reload=(ImageView)view.findViewById(R.id.reload);
        noContentLayout=(RelativeLayout)view.findViewById(R.id.no_content);
        noContentLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);




        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView list_article=getListView();


        list_article.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mCallback.onScroll(view,firstVisibleItem, visibleItemCount,totalItemCount);
            }
        });


        mCallback.getSwipeRefreshLayout(swipeRefreshLayout);
        mCallback.getProgressbar(progressBar);
        mCallback.getReloadLayout(reload,noContentLayout);
        mCallback.getListView(list_article);

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.mreCreate();
            }
        });


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        //do the stuff
        share_layout=(LinearLayout) v.findViewById(R.id.share_layout);
        mCallback.onArticleSelected(position,v);
    }
}
