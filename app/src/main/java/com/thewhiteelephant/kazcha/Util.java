package com.thewhiteelephant.kazcha;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hari on 23/6/16.
 */
public class Util {

    public static String version="1.0";

    public static boolean isDeviceOnline(Context context) {

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isOnline = (networkInfo != null && networkInfo.isConnected());
//        if(!isOnline)
//            Toast.makeText(context, " No internet Connection ", Toast.LENGTH_SHORT).show();

        return isOnline;
    }

    public static Date stringToDate(String dtStart)
    {

        Date date=new Date();
//        String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = format.parse(dtStart);
        } catch (ParseException e) {
            // TODO Auto-generated catch block

            Log.e("Date","Exception");
            e.printStackTrace();
        }
        return date;
    }


    public static String dateInMont(String date)
    {
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("d MMM yyyy hh:mm a");
        String finalDate = sdf.format(getDate(date));

        return finalDate;
    }

    public static String formatDate(String date) {

        boolean sameHour;
        boolean sameDay;
        String finalDate;
        Calendar given = getCalendar(date);


        SimpleDateFormat sdf;
        Calendar now = Calendar.getInstance();

        if(given.getTimeInMillis()>now.getTimeInMillis()) {
            return "0s";
        }


        long difference=now.getTimeInMillis()-given.getTimeInMillis();
        long sec=difference/1000;
        long min=sec/60;
        long hour=min/60;
        long day=hour/24;

        if (day<30)
        {
            finalDate = String.valueOf(day)+"d ago";

            if (hour<24)
            {
                finalDate = String.valueOf(hour)+"h ago";

                if (min<60)
                {
                    finalDate = String.valueOf(min)+"m ago";

                    if (sec<60)
                    {
                        finalDate = String.valueOf(sec)+"s ago";
                    }
                }
            }
        }


//        if (given.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
//            sdf = new SimpleDateFormat("dd MMM");
//            finalDate = sdf.format(getDate(date));
//            if(given.get(Calendar.MONTH) == now.get(Calendar.MONTH) &&
//                    given.get(Calendar.DAY_OF_MONTH) >= now.get(Calendar.DAY_OF_MONTH)-7) {
//                finalDate = String.valueOf(now.get(Calendar.DAY_OF_MONTH)-given.get(Calendar.DAY_OF_MONTH))+"d ago";
//                if(given.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)) {
//                    finalDate =String.valueOf(now.get(Calendar.HOUR_OF_DAY)-given.get(Calendar.HOUR_OF_DAY))+"h ago";
//                    sameHour=compare(now.get(Calendar.HOUR_OF_DAY),given.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),given.get(Calendar.MINUTE));
//                    if(sameHour)
//                        finalDate =String.valueOf(Math.abs((60-given.get(Calendar.MINUTE))+now.get(Calendar.MINUTE)))+"m ago";
//                    if(now.get(Calendar.HOUR_OF_DAY)==(given.get(Calendar.HOUR_OF_DAY))) {
//                        finalDate =String.valueOf(now.get(Calendar.MINUTE)-given.get(Calendar.MINUTE))+"m ago";
//                        if(now.get(Calendar.MINUTE)==given.get(Calendar.MINUTE)) {
//                            finalDate =String.valueOf(now.get(Calendar.SECOND)-given.get(Calendar.SECOND))+"s ago";
//                        }
//                    }
//                }
//            }
//        }
        else
        {
            sdf = new SimpleDateFormat("dd MMM yy");
            finalDate = sdf.format(getDate(date));
        }
        return finalDate;
    }


    public static Calendar getCalendar(String date) {

        Calendar given = Calendar.getInstance();


         given.setTime(getDate(date));

        return given;
    }

    private static boolean compare(int currentHour,int pastHour,int currentMin,int pastMin)
    {
        if((currentHour==pastHour+1))
        {
            int minutediff=(60-pastMin)+currentMin;
            if(minutediff<60)
                return true;
        }

        return false;
    }

    public static Date getDate(String date) {

        Date d1 = null;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (date != null) {
            try {
                d1 = sdf1.parse(date);
            } catch (ParseException e) {}
        }
        if(d1==null) {
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                d1 = sdf2.parse(date);
                d1 = sdf1.parse(sdf1.format(d1));
            } catch (ParseException e) {}
        }
        if(d1==null)
        {

            return new Date();
        }
        else   return d1;
    }
}
