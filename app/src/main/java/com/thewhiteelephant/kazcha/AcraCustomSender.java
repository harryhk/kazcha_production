package com.thewhiteelephant.kazcha;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.thewhiteelephant.kazcha.Activities.SplashActivity;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.acra.util.JSONReportBuilder;
import org.json.JSONObject;


/**
 * Created by rawdata on 17/8/15.
 */
public class AcraCustomSender implements ReportSender {

    Context activity;



    @Override
    public void send(Context context, CrashReportData errorContent) throws ReportSenderException {
        activity=context;
        String crashReport = "";

        try {
            JSONObject object =  errorContent.toJSON();
            Log.e("acra", object.toString());
            crashReport = object.toString();
//

        }catch (JSONReportBuilder.JSONReportException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(context,SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("crash", crashReport);
        context.startActivity(intent);

    }

}
