package com.thewhiteelephant.kazcha.Classes;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thewhiteelephant.kazcha.Activities.MainActivity;
import com.thewhiteelephant.kazcha.Info.ArticleInfo;
import com.thewhiteelephant.kazcha.Util;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    private  static final int DATATBASE_VERSION = 2;
    int count;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATATBASE_VERSION);
        this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS articleInfo " + "(articleId integer primary key,title text, content text, imageUrl text,thumbnailUrl text,website text,websiteUrl text,category integer,date text,thubnail_path text,image_path text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("onUpgrade", oldVersion + " " + newVersion);
        if(newVersion>oldVersion) {
            onCreate(db);
        }
    }

    public boolean insertArticle(ArticleInfo articleInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("articleId", articleInfo.articleId);
        contentValues.put("title",articleInfo.title);
        contentValues.put("content", articleInfo.content);
        contentValues.put("thubnail_path", articleInfo.thubnail_path);
        contentValues.put("thumbnailUrl", articleInfo.thumbnailUrl);
        contentValues.put("date", articleInfo.date);
        contentValues.put("image_path", articleInfo.image_path);
        contentValues.put("imageUrl", articleInfo.imageUrl);
        contentValues.put("website", articleInfo.website);
        contentValues.put("websiteUrl", articleInfo.websiteUrl);
        contentValues.put("category", articleInfo.category);

        db.insert("articleInfo", null, contentValues);
        return true;
    }

    public int getArticleCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor = db.rawQuery("select * from articleInfo", null);
        return cursor.getCount();
    }



    public int getArticleCount1(int catogory) {
        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor;
        if ((catogory==MainActivity.NEW) )
        {
            cursor = db.rawQuery("select * from articleInfo", null);
        }

        else
        {
            cursor = db.rawQuery("select * from articleInfo where category ="+ catogory, null);
        }

        return cursor.getCount();
    }

    public boolean articleExist(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor = db.rawQuery("select articleId from articleInfo", null);
        cursor.moveToFirst();
        int count=cursor.getCount();
        while(cursor.getCount()>0 && !cursor .isAfterLast()){

            int x=cursor.getInt(cursor.getColumnIndex("articleId"));
            if (x==id)
            {
                return true;
            }
            cursor .moveToNext();
        }
        return false;
    }


    public int greatestId()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor = db.rawQuery("select articleId from articleInfo", null);

        cursor.moveToFirst();
        int large=cursor.getInt(cursor.getColumnIndex("articleId"));
        while(cursor.getCount()>0 && !cursor .isAfterLast()){

            int x=cursor.getInt(cursor.getColumnIndex("articleId"));

            if(x>large)
                large=x;

            cursor .moveToNext();
        }
        return large;
    }


    public ArrayList<ArticleInfo> getAllArticles(int id) {
        ArrayList<ArticleInfo> array_list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res;
        if (id==MainActivity.NEW)
        {
            res =  db.rawQuery( "select * from articleInfo", null);
        }
        else {

            res =  db.rawQuery( "select * from articleInfo where category =" + id, null);
        }

        res.moveToFirst();
        while(res.getCount()>0 && !res.isAfterLast()){

            ArticleInfo articleInfo=new ArticleInfo();
            articleInfo.articleId=res.getInt(res.getColumnIndex("articleId"));
            articleInfo.title=res.getString(res.getColumnIndex("title"));
            articleInfo.content=res.getString(res.getColumnIndex("content"));
            articleInfo.imageUrl=res.getString(res.getColumnIndex("imageUrl"));
            articleInfo.thubnail_path =res.getString(res.getColumnIndex("thubnail_path"));
            articleInfo.thumbnailUrl=res.getString(res.getColumnIndex("thumbnailUrl"));
            articleInfo.date=res.getString(res.getColumnIndex("date"));
            articleInfo.dateTime= Util.stringToDate( articleInfo.date);
            articleInfo.image_path=res.getString(res.getColumnIndex("image_path"));
            articleInfo.imageUrl=res.getString(res.getColumnIndex("imageUrl"));
            articleInfo.website=res.getString(res.getColumnIndex("website"));
            articleInfo.websiteUrl=res.getString(res.getColumnIndex("websiteUrl"));
            articleInfo.category=res.getInt(res.getColumnIndex("category"));

            array_list.add(articleInfo);
            res.moveToNext();
        }
        if(!res.isClosed()) res.close();
        return  array_list;
    }



    public void deleteAllArticles()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from articleInfo");
        db.execSQL("vacuum");
        db.close();
    }


}
