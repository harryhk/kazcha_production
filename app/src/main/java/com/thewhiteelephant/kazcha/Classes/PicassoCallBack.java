package com.thewhiteelephant.kazcha.Classes;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by hari on 23/6/16.
 */
public class PicassoCallBack extends Callback.EmptyCallback {
    ImageView imageView;
    String filename;
    public PicassoCallBack(ImageView imageView, String filename) {
        this.imageView = imageView;
        this.filename = filename;

    }

    @Override public void onSuccess() {
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        try {
            ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos1);
            File file = new File(filename);
            FileOutputStream outStream = new FileOutputStream(file);
            outStream.write(baos1.toByteArray());
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onError() {
        Log.e("picasso", "error");
    }
}
